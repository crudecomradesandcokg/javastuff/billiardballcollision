import processing.core.PApplet;
import processing.core.PGraphics;

public class Billiardtisch extends PApplet {
    public static void main(String[] args) {
      PApplet.runSketch(new String[]{""}, new Billiardtisch());
    }

    Ball b1;
    Ball b2;
    public void settings() {
        super.size(600, 600);
    }
    public void setup() {
        super.background(0);
        this.b1 = new Ball(color(17, 50, 230), super.width, super.height);
        this.b2 = new Ball(color(230, 50, 17), super.width, super.height);
    }
    public void draw() {
        super.background(0);
        this.b1.randomPos();
        this.b1.draw(super.g);
        this.b2.randomPos();
        this.b2.draw(super.g);
    }
}