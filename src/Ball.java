import processing.core.PGraphics;

class Ball {
    int x, y;
    int fillColor;
    int widthBound, heightBound;
    Ball(int fillColor, int widthBound, int heightBound) {
        this.fillColor = fillColor;
        this.widthBound  = widthBound;
        this.heightBound = heightBound;
        this.randomPos();
    }
    void randomPos() {
        this.x = (int)(Math.random() * this.widthBound);
        this.y = (int)(Math.random() * this.heightBound);
    }
    void draw(PGraphics g) {
        g.fill(this.fillColor);
        g.ellipse(this.x, this.y, 40, 40);
    }
}